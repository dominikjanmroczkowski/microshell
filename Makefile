make: main.c
	gcc -ansi -Wall main.c -o main
debug: main.c
	gcc -g -ansi -Wall main.c -o debug
clean: main debug
	rm main debug
