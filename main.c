#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <stdarg.h>      
#include <string.h>      
#include <sys/wait.h>
#include <fcntl.h>
#include <utime.h>
#define START_SIZE 80
#define TRUE 1
#define FALSE 0
#define NOEXE -1
#define MAX_PATH 1024
#define BUFFER_SIZE 1024
#define HELP_COMUNICATE "Microshell - Dominik Mroczkowski\nexit - exits microshell\ncd - navitages throught directories\nExecutable needs to be in directory specified by PATH variable to be rewoken\ncp copy files\ntouch creates files\n"

void logTerminalError(char * error) {
	printf("\033[1;31m%s\033[0m", error);
}

char * concat(char * string1, char * string2) {
	int lenght;
	lenght = strlen(string1) + strlen(string2) + 1;
	char * added = calloc(sizeof(char), lenght);
	strcpy(added, string1);
	strcat(added, string2);
	return added;
}

char * getLine() {
	int size = START_SIZE;
	char * line = malloc(sizeof(char) * size);
	
	int counter = 0;
	char c;
	while ('\n' != (c = getchar())) {
	/*	if ('\' == c && ) {
			c = ' ';
		} */
		if (size < counter + 1) {
			size = size + START_SIZE;
			line = realloc(line, sizeof(char) * size);
		}
		line[counter] = c;
		counter++;
	}
	line[counter] = '\0';
	
	return line;
}

static int oneOf(char in, char * table) {
	int i = 0;
	char c;
	while ('\0' != (c = table[i])) {
		if (in == c) {
			return TRUE;
		}
		i++;
	}
	return FALSE;
}

char ** tokenize(char * string, char * delimiters) {
	char ** table = malloc(sizeof(char *) * 80);
	
	int copy = 0, position = 0;
	int tokenStart = 0, tokenEnd = 0, inToken = FALSE;
	int counter = 0;

	if (string[counter] == '\0') {
		goto RETURN;
	}
	while (TRUE) {
		while (TRUE) {
			if ('\0' == string[counter] && !inToken) {
				position++;
				goto RETURN;
			}
			if (!inToken && !oneOf(string[counter], delimiters)) {
				inToken = TRUE;
				tokenStart = counter;
			}
			if (inToken && (oneOf(string[counter], delimiters) || string[counter] == '\0' || 
						string[counter] == '\n')) { 
				inToken = FALSE;
				tokenEnd = (counter - 1);
				counter++;
				break;
			}
			counter++;
		}
		
		table[position] = calloc(sizeof(char), (tokenEnd - tokenStart + 1));
		
		for (; tokenStart <= tokenEnd; tokenStart++) {
			table[position][copy] = string[tokenStart];
			copy++;
		}
		
		position++;
		copy = 0;
	}
RETURN:
	table[position] = NULL;
	return table;
}

char size(char ** tokens) {
	int size = 0;
	while (tokens[size] != NULL) {
		size++;
	}
	return size;
}

void freetokens(char ** tokens) {
	int size = 0;
	while (tokens[size] != null) {
		free(tokens[size]);
		size++;
	}
}

static int fileExist(char * path) {
	struct stat * stats = malloc(sizeof(struct stat));
	int error = stat(path, stats);
	free(stats);
	if (error < 0)
		return FALSE;
	else 
		return TRUE;
}

int searchPath(char ** path, char * executable) {
	if (executable == NULL) {
		return NOEXE;
	}
	
	char * PATH = getenv("PATH");
	char ** paths = tokenize(PATH, ":");

	int i = 0;
	while (NULL != paths[i]) {
		* path = concat(concat(paths[i], "/"), executable);
		if (fileExist(*path)) {
			return TRUE;	
		}
		free(*path);
		i++;
	}
	
	return FALSE;
}

int cd(char * path, char * last) {
	char * current = malloc(sizeof(char) * MAX_PATH);
	getcwd(current, MAX_PATH);
	int error = TRUE;
	char * home = getenv("HOME");
	if (0 == strcmp(path, ".")) {
		return TRUE;
	}
	else if (0 == strcmp(path, "-")) {
		error = chdir(last);
	}	
	else if (0 == strcmp(path, "~")) {
		error = chdir(home);
	}
	else if (path[0] == '/') {
		error = chdir(path);
	} else {
		char * buffer = malloc(sizeof(char) * MAX_PATH);
		getcwd(buffer, MAX_PATH);
		char * concated = concat(buffer, "/");
		char * concated2 = concat(concated, path);
		error = chdir(concated2);
		free(buffer);
		free(concated);
		free(concated2);
	}
	if (0 > error) {
		return FALSE;
	}
	last = current;
	return TRUE;
}

int cp(char * oldFilePath, char * newFilePath) {
	if (FALSE == fileExist(oldFilePath)) {
		printf("Microshell: cp: file %s doesn't exist\n", oldFilePath);
		return FALSE;
	}
	if (TRUE == fileExist(newFilePath)) {
		printf("Microshell: cp: file %s exist\n", oldFilePath);
		return FALSE;
	}
	struct stat fileStat;
	stat(oldFilePath, &fileStat);

	char buffer[1024];
	int size;
	int oldFile = open(oldFilePath, O_RDONLY);
	int newFile = open(newFilePath, O_WRONLY | O_CREAT, fileStat.st_mode);
	while ((size = read(oldFile, &buffer, BUFFER_SIZE)) > 0) {
		write(newFile, &buffer, size);
	}

	close(oldFile);
	close(newFile);

	return TRUE;
}

int touch(char * filePath) {
	int file = open(filePath, O_WRONLY | O_CREAT, 0666);
	close(file);
	utime(filePath, NULL);
	return TRUE;
}

int main(int argc, char **argv) {
	pid_t process;
	int error;
	char * path = calloc(sizeof(char), MAX_PATH);
	char * lastPath = calloc(sizeof(char), MAX_PATH);
	char * currentPath = calloc(sizeof(char), MAX_PATH);
	char ** command;
	int * wstatus = malloc(sizeof(int));
	char * line;
	char * user = getenv("USER");
	char * hostname = malloc(sizeof(char) * 256);
	gethostname(hostname, 256);
	getcwd(currentPath, 256);
	getcwd(lastPath, 256);
	
	while (TRUE) {
		printf("[%s@%s %s]$ ", user, hostname, currentPath);

		line = getLine();
		command = tokenize(line, " \t");
		free(line);
		
		error = searchPath(&path, command[0]);
		if (NOEXE == error) {
			;
		} 
		else if (0 == strcmp(command[0], "exit")) {
			exit(0); 
		}
		else if (0 == strcmp(command[0], "help")) {
			printf(HELP_COMUNICATE); 
		}
		else if (0 == strcmp(command[0], "cp")) {
			if (size(command) > 3) {
				logTerminalError("Microshell: cp: too many arguments\n\tcp usage:\n\tcp [existing file] [new file]\n\033[0m");
			} else if (size(command) < 3) {		
				logTerminalError("Microshell: cp: too few arguments\n\tcp usage:\n\tcp [existing file] [new file]\n");
			} else {
				cp(command[1], command[2]);
			}
		}
		else if (0 == strcmp(command[0], "touch")) {	
			if (size(command) > 2) {
				logTerminalError("Microshell: touch: too many arguments\n\ttouch usage:\n\ttouch [file]\n");
			} else if (size(command) < 2) {		
				logTerminalError("Microshell: touch: too few arguments\n\ttouch usage:\n\ttouch [file]\n");
			} else {
				touch(command[1]);
			}
		}
		else if (0 == strcmp(command[0], "cd")) {
			if (size(command) > 2) {
				logTerminalError("Microshell: cd: too many arguments\n");
			} else if (size(command) < 2) {
				logTerminalError("Microshell: cd: cd needs at least 1 argument\n");
			}
			else if (TRUE == cd(command[1], lastPath)) {
				getcwd(currentPath, 256);
			} else {
				logTerminalError("Directory doesn't exist\n");
			}
		}
		else if (FALSE == error) {
			logTerminalError("Microshell: command not found\n");
		} else {
			process = fork();
			if (0 == process) {
				if (TRUE == error) {
					execv(path, command);
				}
			}
			waitpid(process, wstatus, 0);
		}
		freeTokens(command);
	}
	return 0;
}
